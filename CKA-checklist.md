CKA Checklist

Cluster Architecture, Installation and Configuration (25%)
- [ ] Manage role based access control (RBAC)
- [ ] Use Kubeadm to install a basic cluster
- [ ] Manage a highly-available Kubernetes cluster
- [ ] Provision underlying infrastructure to deploy a Kubernetes cluster
- [ ] Perform a version upgrade on a Kubernetes cluster using Kubeadm
- [ ] Implement etcd backup and restore

Workloads and Scheduling (15%)
- [ ] Understand deployments and how to perform rolling update and rollbacks
- [ ] Use ConfigMaps and Secrets to configure applications
- [ ] Know how to scale applications
- [ ] Understand the primitives used to create robust, self-healing, application deployments
- [ ] Understand how resource limits can affect Pod scheduling
- [ ] Awareness of manifest management and common templating tools

Services and Networking (20%)
- [ ] Understand host networking configuration on the cluster nodes
- [ ] Understand connectivity between Pods
- [ ] Understand ClusterIP, NodePort, LoadBalancer service types and endpoints
- [ ] Know how to use Ingress controllers and Ingress resources
- [ ] Know how to configure and use CoreDNS
- [ ] Choose an appropriate container network interface plugin

Storage (10%)
- [ ] Understand storage classes, persistent volumes
- [ ] Understand volume modes, access modes and reclaim policies for volumes
- [ ] Understand persistent volume claims primitives
- [ ] Know how to configure applications with persistent storage

Troubleshooting (30%)
	◦	Evaluate cluster and node logging
	◦	Understand how to monitor applications
	◦	Manage container stdout and stderr logs
	◦	Troubleshoot cluster component failure
	◦	Troubleshoot networking



Resources to satisfy the check list above
Cluster Architecture, Installation and Configuration (25%)
- https://devopscube.com/setup-kubernetes-cluster-kubeadm/
- https://kubernetes.io/docs/reference/access-authn-authz/rbac/
- https://www.youtube.com/watch?v=NlDdUknt96E
- https://www.redhat.com/en/topics/containers/what-is-kubernetes-cluster-management
- https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/
- https://ystatit.medium.com/backup-and-restore-kubernetes-etcd-on-the-same-control-plane-node-20f4e78803cb

