export AWS_REGION=us-east-1
export NAME=k8s-cluster-from-ground-up

#### copy *.kubeconfig files to worker nodes
for i in 0 1 2; do
  instance="${NAME}-worker-${i}"
  external_ip=$(aws ec2 describe-instances \
    --filters "Name=tag:Name,Values=${instance}" \
    --output text --query 'Reservations[].Instances[].PublicIpAddress')
  scp -i ../ssh/${NAME}.id_rsa \
     kube-proxy.kubeconfig ${instance}.kubeconfig ubuntu@${external_ip}:~/; \
done

#### copy *.kubeconfig files to master nodes
for i in 0 1 2; do
instance="${NAME}-master-${i}" \
  external_ip=$(aws ec2 describe-instances \
    --filters "Name=tag:Name,Values=${instance}" \
    --output text --query 'Reservations[].Instances[].PublicIpAddress')
  scp -i ../ssh/${NAME}.id_rsa \
     admin.kubeconfig kube-controller-manager.kubeconfig  kube-proxy.kubeconfig \
     kube-scheduler.kubeconfig ubuntu@${external_ip}:~/;
done


### ETCD key generation
ETCD_ENCRYPTION_KEY=$(head -c 32 /dev/urandom | base64) 

## connect to master
master_1_ip=$(aws ec2 describe-instances \
--filters "Name=tag:Name,Values=${NAME}-master-0" \
--output text --query 'Reservations[].Instances[].PublicIpAddress')
ssh -i ../ssh/${NAME}.id_rsa ubuntu@${master_1_ip}

Master-2

master_2_ip=$(aws ec2 describe-instances \
--filters "Name=tag:Name,Values=${NAME}-master-1" \
--output text --query 'Reservations[].Instances[].PublicIpAddress')
ssh -i ../ssh/${NAME}.id_rsa ubuntu@${master_2_ip}

Master-3

master_3_ip=$(aws ec2 describe-instances \
--filters "Name=tag:Name,Values=${NAME}-master-2" \
--output text --query 'Reservations[].Instances[].PublicIpAddress')
ssh -i ../ssh/${NAME}.id_rsa ubuntu@${master_3_ip}


#### copy  encryption-config.yaml files to the control plane
for i in 0 1 2; do
instance="${NAME}-master-${i}" \
  external_ip=$(aws ec2 describe-instances \
    --filters "Name=tag:Name,Values=${instance}" \
    --output text --query 'Reservations[].Instances[].PublicIpAddress')
  scp -i ../ssh/${NAME}.id_rsa \
      encryption-config.yaml ubuntu@${external_ip}:~/;
done

export INTERNAL_IP=$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)
export KUBERNETES_API_SERVER_ADDRESS="k8s-api-server.svc.darey.io"



#### SSH into the worker nodes

Worker-1

worker_1_ip=$(aws ec2 describe-instances \
--filters "Name=tag:Name,Values=${NAME}-worker-0" \
--output text --query 'Reservations[].Instances[].PublicIpAddress')
ssh -i ../ssh/${NAME}.id_rsa  ubuntu@${worker_1_ip}

Worker-2

worker_2_ip=$(aws ec2 describe-instances \
--filters "Name=tag:Name,Values=${NAME}-worker-1" \
--output text --query 'Reservations[].Instances[].PublicIpAddress')
ssh -i ../ssh/${NAME}.id_rsa  ubuntu@${worker_2_ip}

Worker-3

worker_3_ip=$(aws ec2 describe-instances \
--filters "Name=tag:Name,Values=${NAME}-worker-2" \
--output text --query 'Reservations[].Instances[].PublicIpAddress')
ssh -i ../ssh/${NAME}.id_rsa  ubuntu@${worker_3_ip}


# management commands

- sudo systemctl status kube-apiserver  

- sudo systemctl status kube-controller-manager

- sudo systemctl status kube-scheduler

- sudo systemctl status kubelet

- sudo systemctl status kube-proxy


- To get the cluster details run:
   kubectl cluster-info --kubeconfig admin.kubeconfig
   kubectl cluster-info dump --kubeconfig admin.kubeconfig

- To get the current namespaces:
    kubectl get namespaces --kubeconfig admin.kubeconfig
    Tasks:
      - create ns and switch ns
      - create pods in new ns

- To reach the Kubernetes API Server publicly
   curl --cacert /var/lib/kubernetes/ca.pem https://$INTERNAL_IP:6443/version

- To get the status of each component:
   kubectl get componentstatuses --kubeconfig admin.kubeconfig
- get nodes
   kubectl get nodes  --kubeconfig admin.kubeconfig -o wide


shell scripts for creating and distributing certs to be run locally, abd copy other shell script for bootstapping to respective nodes

shell script to bootstrap etcd

shell script to bootstap control plane
