data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name    = "root-device-type"
    values  =   ["ebs"]
  }
  
  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "control_plane" {
  count                       = 3
  ami                         = data.aws_ami.ubuntu.id
  associate_public_ip_address = true
  key_name                    = var.key_name
  subnet_id                   = var.subnet_id
  private_ip                  = var.control_plane_ip[count.index]
  vpc_security_group_ids      = [var.security_groups]
  instance_type               = "t2.micro"

  tags = {
    Name = format("k8s-cluster-from-ground-up-master-%s", count.index)
  }
}


resource "aws_instance" "worker_nodes" {
  count                       = 3
  ami                         = data.aws_ami.ubuntu.id
  associate_public_ip_address = true
  key_name                    = var.key_name
  subnet_id                   = var.subnet_id
  private_ip                  = var.worker_node_ip[count.index]
  vpc_security_group_ids      = [var.security_groups]
  instance_type               = "t2.micro"

  tags = {
    Name =format("k8s-cluster-from-ground-up-worker-%s", count.index)
  }
}