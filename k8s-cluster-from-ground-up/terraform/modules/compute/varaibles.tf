variable "cluster_name" {
  type        = string
  description = "Name of the cluster"
}


variable "control_plane_ip" {
  description = "Ip for the control plane"
}


variable "worker_node_ip" {
  description = "Ip for the control plane"
}

variable "key_name" {
  description = "key pair for ssh access"
}

variable "subnet_id" {
  description = "compute subnet"
}

variable "security_groups" {
  
}