output "control_plane_instance" {
  value       = aws_instance.control_plane[*].id
  description = "All control plane instance"
}