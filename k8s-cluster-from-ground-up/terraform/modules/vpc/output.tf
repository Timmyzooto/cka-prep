output "vpc_id" {
  value = aws_vpc.main.id
}

output "subnet_cidr_block" {
  value = aws_subnet.subnet_cidr_block.id
}