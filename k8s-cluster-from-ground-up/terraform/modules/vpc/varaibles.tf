variable "cidr_block" {
  type        = string
  description = "VPC cidr block"
}

variable "subnet_cidr_block" {
  type        = string
  description = "subnet cidr block"
}

variable "cluster_name" {
  type        = string
  description = "Name of the cluster"
}

variable "subnet_availability_zone" {
  type        = string
  description = "avaialbility zone for the subnets"
}