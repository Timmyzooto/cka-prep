# ALB, target group

resource "aws_lb" "kubernetes" {
  name               = var.cluster_name
  internal           = false
  load_balancer_type = "network"
  subnets            = [var.subnet_public]

  enable_deletion_protection = false

  tags = {
    Name = var.cluster_name
  }
}

resource "aws_lb_target_group" "kubernetes-tgt" {
  name     = var.cluster_name
  port     = 6443
  protocol = "TCP"
  vpc_id   = var.vpc_id

   health_check {
    interval = 30
    port = 6443
    protocol = "TCP"
  }

}

resource "aws_lb_target_group_attachment" "test" {
  count            = 3
  target_group_arn = aws_lb_target_group.kubernetes-tgt.arn
  target_id        = var.control_plane_instance[count.index]
  port             = "6443"
}


resource "aws_lb_listener" "kubernetes-tgt-list" {
  load_balancer_arn = aws_lb.kubernetes.arn
  port              = "6443"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.kubernetes-tgt.arn
  }
}