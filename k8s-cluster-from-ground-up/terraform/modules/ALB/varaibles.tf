variable "cluster_name" {
  type        = string
  description = "Name of the cluster"
}

variable "subnet_public" {
  description = "public subents for ALB"
}


variable "control_plane_instance" {
  description = "IP addresses of the control plane"
}


variable "vpc_id" {
  description = "IP address of the control plane"
}