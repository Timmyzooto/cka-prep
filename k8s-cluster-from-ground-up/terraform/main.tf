module "VPC" {
  source                   = "./modules/vpc/"
  cidr_block               = var.cidr_block
  cluster_name             = var.cluster_name
  subnet_cidr_block        = var.subnet_cidr_block
  subnet_availability_zone = var.subnet_availability_zone
}


module "ALB" {
  source                 = "./modules/ALB/"
  vpc_id                 = module.VPC.vpc_id
  cluster_name           = var.cluster_name
  subnet_public          = module.VPC.subnet_cidr_block
  control_plane_instance = module.compute.control_plane_instance

}

module "security" {
  source       = "./modules/security/"
  vpc_id       = module.VPC.vpc_id
  cluster_name = var.cluster_name
}

module "compute" {
  source           = "./modules/compute/"
  cluster_name     = var.cluster_name
  control_plane_ip = [for i in range(10, 13, 1) : cidrhost("172.31.0.0/24", i)]
  worker_node_ip   = [for i in range(20, 23, 1) : cidrhost("172.31.0.0/24", i)]
  key_name         = var.key_name
  subnet_id        = module.VPC.subnet_cidr_block
  security_groups  = module.security.security_group
}